const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
// const imagemin = require('gulp-imagemin');

// const src = gulp.src;
// const dest = gulp.dest;
// const { src, dest } = require('gulp');
function copyHtml() {
    return gulp.src('src/index.html')
        .pipe(gulp.dest('dist/'))
}

function copyJS() {
    return gulp.src('src/main.js')
        .pipe(gulp.dest('dist/'))
}

function watch() {
    gulp.watch('src/index.html', copyHtml).on('change', browserSync.reload);
    gulp.watch('src/main.js', copyJS).on('change', browserSync.reload);
    gulp.watch('src/styles/*.scss', sassToCss)
}

function serve() {
    browserSync.init({
        server: './dist'
    });
    watch();
}

function sassToCss() {
    return gulp.src('src/styles/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream())
}

// function minImage() {
//     return gulp.src('src/img/**/**/*')
//           .pipe(imagemin())
//           .pipe(gulp.dest('dist/img'));
// }

exports.build = gulp.series(copyHtml, sassToCss, copyJS);
exports.watch = watch;
exports.sass = sassToCss;
exports.serve = serve;
